import styles from './List.module.css'

export const List = ({ data }) => {

  return (
    <div className={styles.container}>
      <div className={styles.title}>Active Item : {'-'}</div>
      <div className={styles.list}>
        {data.map((i, index) => (
          <ListItem
            key={`item-${index}`}
            name={i.name}
          />
        ))}
      </div>
    </div>
  )
}

const ListItem = ({ name }) => {

  console.log('render in child', name)

  return (
    <div
      className={styles.item}
    >
      {name}
      <ActiveBadge />
    </div>
  )
}

const ActiveBadge = ({ active }) => {
  if (!active) return null

  return (
    <div className={styles.active} />
  )
}